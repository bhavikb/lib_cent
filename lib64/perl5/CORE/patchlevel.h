/*    patchlevel.h
 *
 *    Copyright (C) 1993, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002,
 *    2003, 2004, 2005, 2006, 2007, 2008, 2009, by Larry Wall and others
 *
 *    You may distribute under the terms of either the GNU General Public
 *    License or the Artistic License, as specified in the README file.
 *
 */

#ifndef __PATCHLEVEL_H_INCLUDED__

/* do not adjust the whitespace! Configure expects the numbers to be
 * exactly on the third column */

#define PERL_REVISION	5		/* age */
#define PERL_VERSION	10		/* epoch */
#define PERL_SUBVERSION	1		/* generation */

/* The following numbers describe the earliest compatible version of
   Perl ("compatibility" here being defined as sufficient binary/API
   compatibility to run XS code built with the older version).
   Normally this should not change across maintenance releases.

   Note that this only refers to an out-of-the-box build.  Many non-default
   options such as usemultiplicity tend to break binary compatibility
   more often.

   This is used by Configure et al to figure out 
   PERL_INC_VERSION_LIST, which lists version libraries
   to include in @INC.  See INSTALL for how this works.
*/
#define PERL_API_REVISION	5	/* Adjust manually as needed.  */
#define PERL_API_VERSION	10	/* Adjust manually as needed.  */
#define PERL_API_SUBVERSION	0	/* Adjust manually as needed.  */
/*
   XXX Note:  The selection of non-default Configure options, such
   as -Duselonglong may invalidate these settings.  Currently, Configure
   does not adequately test for this.   A.D.  Jan 13, 2000
*/

#define __PATCHLEVEL_H_INCLUDED__
#endif

/*
	local_patches -- list of locally applied less-than-subversion patches.
	If you're distributing such a patch, please give it a name and a
	one-line description, placed just before the last NULL in the array
	below.  If your patch fixes a bug in the perlbug database, please
	mention the bugid.  If your patch *IS* dependent on a prior patch,
	please place your applied patch line after its dependencies. This
	will help tracking of patch dependencies.

	Please either use 'diff --unified=0' if your diff supports
	that or edit the hunk of the diff output which adds your patch
	to this list, to remove context lines which would give patch
	problems. For instance, if the original context diff is

	   *** patchlevel.h.orig	<date here>
	   --- patchlevel.h	<date here>
	   *** 38,43 ***
	   --- 38,44 ---
	     	,"FOO1235 - some patch"
	     	,"BAR3141 - another patch"
	     	,"BAZ2718 - and another patch"
	   + 	,"MINE001 - my new patch"
	     	,NULL
	     };
	
	please change it to 
	   *** patchlevel.h.orig	<date here>
	   --- patchlevel.h	<date here>
	   *** 41,43 ***
	   --- 41,44 ---
	   + 	,"MINE001 - my new patch"
	     	,NULL
	     };
	
	(Note changes to line numbers as well as removal of context lines.)
	This will prevent patch from choking if someone has previously
	applied different patches than you.

        History has shown that nobody distributes patches that also
        modify patchlevel.h. Do it yourself. The following perl
        program can be used to add a comment to patchlevel.h:

#!perl
die "Usage: perl -x patchlevel.h comment ..." unless @ARGV;
open PLIN, "patchlevel.h" or die "Couldn't open patchlevel.h : $!";
open PLOUT, ">patchlevel.new" or die "Couldn't write on patchlevel.new : $!";
my $seen=0;
while (<PLIN>) {
    if (/\t,NULL/ and $seen) {
       while (my $c = shift @ARGV){
            print PLOUT qq{\t,"$c"\n};
       }
    }
    $seen++ if /local_patches\[\]/;
    print PLOUT;
}
close PLOUT or die "Couldn't close filehandle writing to patchlevel.new : $!";
close PLIN or die "Couldn't close filehandle reading from patchlevel.h : $!";
close DATA; # needed to allow unlink to work win32.
unlink "patchlevel.bak" or warn "Couldn't unlink patchlevel.bak : $!"
  if -e "patchlevel.bak";
rename "patchlevel.h", "patchlevel.bak" or
  die "Couldn't rename patchlevel.h to patchlevel.bak : $!";
rename "patchlevel.new", "patchlevel.h" or
  die "Couldn't rename patchlevel.new to patchlevel.h : $!";
__END__

Please keep empty lines below so that context diffs of this file do
not ever collect the lines belonging to local_patches() into the same
hunk.

 */

#if !defined(PERL_PATCHLEVEL_H_IMPLICIT) && !defined(LOCAL_PATCH_COUNT)
#  if defined(PERL_IS_MINIPERL)
#    define PERL_PATCHNUM "UNKNOWN-miniperl"
#    define PERL_GIT_UNCOMMITTED_CHANGES ,"UNKNOWN"
#    define PERL_GIT_UNPUSHED_COMMITS /*leave-this-comment*/
#  elif defined(PERL_MICRO)
#    define PERL_PATCHNUM "UNKNOWN-microperl"
#    define PERL_GIT_UNCOMMITTED_CHANGES ,"UNKNOWN"
#    define PERL_GIT_UNPUSHED_COMMITS /*leave-this-comment*/
#  else
#include "git_version.h"
#  endif
static const char * const local_patches[] = {
	NULL
	PERL_GIT_UNPUSHED_COMMITS    	/* do not remove this line */
        PERL_GIT_UNCOMMITTED_CHANGES	/* do not remove this line */
	,"RHEL Patch0: Sys-Syslog has wrong path (build failure) "
	,"RHEL Patch1: Permit suidperl to install as nonroot"
	,"RHEL Patch2: Removes date check, Fedora/RHEL specific"
	,"RHEL Patch3: Work around annoying rpath issue"
	,"RHEL Patch4: support for libdir64"
	,"RHEL Patch5: use libresolv instead of libbind"
	,"RHEL Patch6: USE_MM_LD_RUN_PATH"
	,"RHEL Patch7: Skip hostname tests, due to builders not being network capable"
	,"RHEL Patch8: Dont run one io test due to random builder failures"
	,"RHEL Patch9: Reorder @INC and remove duplicates"
	,"RHEL Patch10: fix RT 39060, errno incorrectly set in perlio"
	,"RHEL Patch11: much better swap logic to support reentrancy and fix assert failure"
	,"RHEL Patch12: backward compatibility for the trasition"
	,"RHEL Patch13: CVE_2009_3626"
	,"RHEL Patch14: man page location in /usr/share"
	,"RHEL Patch15: perlvar pod rendered correctly"
	,"RHEL Patch16: set default CPAN install into site"
	,"RHEL Patch17: remove unsupported fork as option from documentation"
	,"RHEL Patch18: fix RT 77352: Memory leaks in threaded Perl"
	,"RHEL Patch19: CVE-2011-1487 RT#87336"
	,"RHEL Patch20: code injection vulnerability in Digest->new()"
	,"RHEL Patch21: CVE-2011-2939 heap overflow while decoding Unicode string"
	,"RHEL Patch22: Fix POSIX::strftime memory leak"
	,"RHEL Patch23: Do not wrap count argument of Perl_repeatcpy()"
	,"RHEL Patch24: Do not wrap count argument of Perl_repeatcpy() on i386"
	,"RHEL Patch25: Fix CVE-2013-1667 DoS in rehashing code"
	,"RHEL Patch26: Move format to prototyped subroutine properly [1]"
	,"RHEL Patch27: Move format to prototyped subroutine properly [2]"
	,"RHEL Patch28: Move format to prototyped subroutine properly [3]"
	,"RHEL Patch29: Move format to prototyped subroutine properly [4]"
	,"RHEL Patch30: Move format to prototyped subroutine properly [5]"
	,"RHEL Patch31: Move format to prototyped subroutine properly [6]"
	,"RHEL Patch32: Fix memory leak when accessing non-matched named captures"
	,"RHEL Patch33: Do not crash if declared DESTROY method has not been defined"
	,"RHEL Patch34: Fix find2perl to translate ? glob properly"
	,"RHEL Patch35: Fix perl segfaults in Perl_csighandler with custom signal handler"
	,"RHEL Patch100: Update ExtUtils::CBuilder to 0.27"
	,"RHEL Patch101: Update File::Path to 2.08"
	,"RHEL Patch102: Update Module::Build to 0.35"
	,"RHEL Patch103: Update Parse::CPAN::Meta::version to 1.40"
	,"RHEL Patch104: Update Archive::Extract to 0.38"
	,"RHEL Patch105: Update Archive::Tar to 1.58"
	,"RHEL Patch106: Update ExtUtils::ParseXS to 2.2003.0"
	,"RHEL Patch107: Update File::Fetch to 0.26"
	,"RHEL Patch108: Update IPC::Cmd to 0.56"
	,"RHEL Patch109: Update Pod::Simple to 3.13"
	,"RHEL Patch110: Update Safe to 2.27"
	,"RHEL Patch111: Update CGI to 3.51"
	,"RHEL Patch112: Update Time::HiRes to 1.9721"
	,"RHEL Patch113: Update threads to 1.82"
	,"RHEL Patch114: Update IO-Compress to 2.021"
	,"RHEL Patch115: Update Compress-Raw-Bzip2 to 2.021"
	,"RHEL Patch116: Update Compress-Raw-Zlib to 2.021"
	,"RHEL Patch201: Fix Archive::Tar to return correct code on error"
	,"RHEL Patch202: Fix IO::Uncompress::Unzip::nextStream to return 0 on last stream"
	,"RHEL Patch203: Fix CVE-2012-5526 new-lines in Cookie and P3P headers"
	,"RHEL Patch204: Fix CVE-2012-6329 code execution via Locale::Maketext"
	,"RHEL Patch205: Set output for Pod::Man and Pod::Text parse_file() methods"
	,"RHEL Patch206: Make Getopt::Long::GetOptions() compatible with XML::Simple"
	,"RHEL Patch207: Make *DBM_File desctructors thread-safe (RT#61912)"
	,"RHEL Patch208: Fix backslash interpolation in Locale::Maketext (RT#120457)"
	,"RHEL Patch209: Do not ignore single-letter-named packages by Module::Plugable (CPAN RT#89680)"
	,"RHEL Patch210: Do not save PAX headers into PaxHeader subdirectories by Archive::Tar (CPAN RT#64038)"
	,"RHEL Patch211: Fix possible crash on uninitialized Digest::SHA object (RT#121421)"
	,NULL
};



/* Initial space prevents this variable from being inserted in config.sh  */
#  define	LOCAL_PATCH_COUNT	\
	((int)(sizeof(local_patches)/sizeof(local_patches[0])-2))

/* the old terms of reference, add them only when explicitly included */
#define PATCHLEVEL		PERL_VERSION
#undef  SUBVERSION		/* OS/390 has a SUBVERSION in a system header */
#define SUBVERSION		PERL_SUBVERSION
#endif
